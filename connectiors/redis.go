package connectiors

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/redis-test/models"
	"time"
)

// #todo redis sentinal service

type RedisService struct {
	client *redis.Client
}

func Connect(cfg *models.Config, client *redis.Client) *RedisService {
	return &RedisService{
		client: client,
	}
}

func (redis *RedisService) SetValue(key string, value string, expiration time.Duration) error {

	err := redis.client.Set(key, value, expiration).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisService :: Connect :: SetValue :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}

func (redis *RedisService) SetJSONValue(key string, value interface{}, expiration time.Duration) error {

	b, err := json.Marshal(value)
	if err != nil {
		return err
	}

	err = redis.client.Set(key, string(b), expiration).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisService :: Connect :: SetValue :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}

func (redis *RedisService) GetJSONValue(key string) (interface{}, error) {
	data, err := redis.client.Get(key).Result()
	if err != nil {
		return nil, err
	}

	var jsonData interface{}
	json.Unmarshal([]byte(data), &jsonData)
	return jsonData, nil

}


func (redis *RedisService) GetValue(key string, expire bool) (string, error) {
	if expire {
		// Expire after 1 minute
		redis.SetValue(key, "expiring..", time.Second)
		return redis.client.Get(key).Result()
	}
	return redis.client.Get(key).Result()
}
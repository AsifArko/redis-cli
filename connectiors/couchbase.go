package connectiors

import (
	"fmt"
	"github.com/couchbase/gocb"
	"gitlab.com/redis-test/models"
)

type Couchbase struct {
	*gocb.Bucket
}

func GetCouchbaseConnection(cfg *models.Config) (*Couchbase, error) {

	//Cluster Init
	cluster, err := gocb.Connect(fmt.Sprintf("couchbase://%s", cfg.CouchbaseHost))
	if err != nil {
		return nil, errorMessage(err, "Init Cluster")
	}

	//Creating the authenticator
	authenticator := gocb.PasswordAuthenticator{
		Username: cfg.NosqlUser,
		Password: cfg.NosqlPassword,
	}

	//Cluster authentication
	err = cluster.Authenticate(authenticator)
	if err != nil {
		return nil, errorMessage(err, "Cluster Authentication ")
	}

	//Opens up the bucket
	bucket, err := cluster.OpenBucket(cfg.BucketName, "")
	if err != nil {
		return nil, errorMessage(err, "Open Bucket")
	}

	fmt.Printf("\nConnected to Couchbase at %s:%s\n\n", cfg.CouchbaseHost, cfg.CouchbasePort)

	return &Couchbase{bucket}, nil
}

func errorMessage(err error, context string) error {
	message := fmt.Sprintf("Connectiors :: Couchbase :: %s :: %s", context, err.Error())
	fmt.Println(message)
	return err
}

package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/redis-test/connectiors"
	"gitlab.com/redis-test/models"
	"io/ioutil"
	"net/http"
)

var cfg *models.Config

func init() {
	cfg = &models.Config{
		RedisHost:     "localhost",
		RedisPort:     "6379",
		RedisPassword: "123456",

		//Couchbase configuration
		CouchbaseHost: "localhost",
		CouchbasePort: "8091",
		NosqlUser:     "Administrator",
		NosqlPassword: "123456789",
		BucketName:    "locations",
	}
}

func main() {

	var redisConn *redis.Client
	if cfg.RedisPassword != "" {
		// #todo implement tls connection
		redisConn = redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
			Password: cfg.RedisPassword,
		})
	} else {
		redisConn = redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
		})
	}

	ping, err := redisConn.Ping().Result()
	if err != nil {
		fmt.Println(fmt.Sprintf("Redis Service is Offline :  %s \n", err.Error()))
	}

	fmt.Println(ping)

	divisions, _ := GetAllDivisions(cfg)
	//fmt.Println(divisions)

	//Insert division with districts inside it
	for _ , div:= range divisions.Divisions{
		div.Districts = GetDistrictsUnderADivision(div.Code)
		for _ , district := range div.Districts{
			GetAreasUnderADistrict(div.Code , &district , redisConn)
		}
	}
}

func GetAreasUnderADistrict(divId string , district *models.District , redisConn *redis.Client)  {
	key := fmt.Sprintf("division::%s::district::%s",divId , district.Code)

	res := redisConn.Get(key)
	str,_ := res.Result()
	fmt.Println(str)



	nosql, err := connectiors.GetCouchbaseConnection(cfg)
	if err != nil {
		panic(err)
	}

	_, err = nosql.Insert(key , str, 0)
	if err != nil {
		panic(err)
	}
}

func GetDistrictsUnderADivision(did string)  []models.District {

	url := fmt.Sprintf("https://api.shophobe.com/locations?did=%s",did)
	res, err := http.Get(url)
	if err != nil{
		panic(err)
	}

	b , _ := ioutil.ReadAll(res.Body)
	var resp models.Response
	err = json.Unmarshal(b , &resp)
	if err != nil{
		panic(err)
	}

	var districts []models.District
	for _ , dist:= range resp.Message.Names{
		b , _ := json.Marshal(dist)

		var district models.District
		district.Type = "district"
		err := json.Unmarshal(b , &district)
		if err != nil{
			panic(err)
		}
		districts= append(districts, district)
	}
	return districts
}


func InsertDivision(cfg *models.Config , division *models.Division)  {
	nosql, err := connectiors.GetCouchbaseConnection(cfg)
	if err != nil {
		panic(err)
	}

	key := fmt.Sprintf("%s::%s","division",division.Code)
	_, err = nosql.Insert(key , division, 0)
	if err != nil {
		panic(err)
	}
}

func GetAllDivisions(cfg *models.Config) (*models.Divisions, error) {

	nosql, err := connectiors.GetCouchbaseConnection(cfg)
	if err != nil {
		return nil, err
	}

	var divisions models.Divisions
	_, err = nosql.Get("divisions", &divisions)
	if err != nil {
		return nil, err
	}

	return &divisions, nil
}

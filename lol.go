package main

import (
	"fmt"
	"github.com/go-redis/redis"
)

func main()  {
	var redisConn *redis.Client
	if cfg.RedisPassword != "" {
		// #todo implement tls connection
		redisConn = redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
			Password: cfg.RedisPassword,
		})
	} else {
		redisConn = redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
		})
	}

	ping, err := redisConn.Ping().Result()
	if err != nil {
		fmt.Println(fmt.Sprintf("Redis Service is Offline :  %s \n", err.Error()))
	}

	fmt.Println(ping)


}

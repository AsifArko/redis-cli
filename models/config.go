package models

type Config struct {

	// Redis Credential
	RedisHost     string
	RedisPort     string
	RedisUser     string
	RedisPassword string

	//Couchbase Configuration
	CouchbaseHost  string
	CouchbasePort  string
	NosqlUser      string
	NosqlPassword  string
	BucketName     string
	BucketPassword string
}

package models

type Response struct {
	Code    uint32  `json:"code"`
	Message Message `json:"message"`
}

type Message struct {
	Names []Divs `json:"names"`
	Type  string `json:"type"`
}

type Divs struct {
	Code    string `json:"code"`
	Display string `json:"display"`
}

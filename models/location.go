package models

type Divisions struct {
	Type      string     `json:"type"`
	Divisions []Division `json:"divisions"`
}

type Division struct {
	Code      string     `json:"code"`
	Type      string     `json:"type"`
	Display   string     `json:"display"`
	Referance string     `json:"-"`
	Districts []District `json:"districts"`
}

type District struct {
	Code      string `json:"code"`
	Type      string `json:"type"`
	Display   string `json:"display"`
	Referance string `json:"-"`
}
